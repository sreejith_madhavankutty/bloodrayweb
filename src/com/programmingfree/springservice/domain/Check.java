package com.programmingfree.springservice.domain;

public class Check {
  private int tag;
  private int status;
  public Check()
  {
    tag=0;
  status=0;
  }
  public int gettag(int a) {
	  tag=a;
	  return tag;
	 }
  public int getstat(int b) {
	  status=b;
	  return status;
	  
	 }
  @Override
  public String toString() {
   return "Check [tag=" + tag + ", status=" + status
     + "]";
  }
   
}

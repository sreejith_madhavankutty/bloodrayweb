package com.programmingfree.springservice.domain;

public class RequestBlood {

	 private int userId;
	 private String bloodgrp;
	 private int amount;
	 private int reId;
	 private String location;
	 private int coordinatorid;
	 private String status;
	 public int getUserid() {
	  return userId;
	 }
	 public void setUserid(int userid) {
	  this.userId = userid;
	 }
	 public int getreid() {
		  return reId;
		 }
		 public void setreid(int rid) {
		  this.reId = rid;
		 }
	 public String getbloodgrp() {
	  return bloodgrp;
	 }
	 public void setbloodgrp(String bldgrp) {
	  this.bloodgrp = bldgrp;
	 }
	 public int getamount() {
	  return amount;
	 }
	 public void setamount(int amt) {
	  this.amount = amt;
	 }
	 
	 
	 public String getstatus() {
	  return status;
	 }
	 public void setstatus(String stat) {
	  this.status = stat;
	 }
	 public int getcordinatorid() {
		  return coordinatorid;
		 }
	 public void setcordinatorid(int cid) {
		  this.coordinatorid = cid;
		 }
     public String getlocation() {
			  return location;
			 }
     public void setlocation(String loc) {
			  this.location = loc;
			 }
	 @Override
	 public String toString() {
	  return "User [userId=" + userId + ",reId="+reId+" bloodgrp=" + bloodgrp
	    + ", amount=" + amount+ ", loaction="
	    + location + ",status="+status+",coordinatorid="+coordinatorid+"]";
	 }
	  
	  
	}


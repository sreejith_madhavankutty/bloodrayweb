package com.programmingfree.springservice.domain;

public class Event {
 private int eno;
 private String etitle;
 private String location;
 private String event_date;
 private String description;

 public void seteno(int no)
 {
	 this.eno=no;
 }
 public int getno()
 {
   return eno;	 
 }
 public void setloc(String loc)
 {
	 this.location=loc;
	 
 }
 public String getloc()
 {
	 return location;
 }
 public void setdate(String date)
 {
	 this.event_date=date;
	 
 }
 public String getdate()
 {
	 return event_date;
 }
 public void setdesc(String desc)
 {
	 this.description=desc;
	 
 }
 public String getdesc()
 {
	 return description;
 }
 public void settitle(String title)
 {
	 this.etitle=title;
	 
 }
 public String getitle()
 {
	 return etitle;
 }
}

package com.programmingfree.springservice.dao;



import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import com.programmingfree.springservice.domain.RequestBlood;
import com.programmingfree.springservice.domain.User;
import com.programmingfree.springservice.utility.DBUtility;


public class RequestBloodService {
 
 private Connection connection;
 int checked=0;

 public RequestBloodService() {
  connection = DBUtility.getConnection();
 }
 public void insertRequests(RequestBlood rb){
	 try {
		 System.out.println("HELLOOOOOOOOO"+connection) ;
		 PreparedStatement preparedStatement = connection
		     .prepareStatement("insert into request (userid,reid,bg_needed,location,no_of_units,rstatus,co_ordinator_id)" + " values (?,?, ?,?,?,?,?)");
		   System.out.println("Request:"+rb.getbloodgrp());
		   preparedStatement.setInt(1,rb.getUserid());

		   preparedStatement.setInt(2,rb.getreid());
		   preparedStatement.setString(3, rb.getbloodgrp());
		   System.out.println("Eyuiop"); 
		   preparedStatement.setString(4, rb.getlocation());

		   preparedStatement.setInt(5, rb.getamount());  
		   preparedStatement.setString(6, rb.getstatus());
		   preparedStatement.setInt(7, rb.getcordinatorid());   
		   preparedStatement.execute();
		   
		   System.out.println("OUt...s");
	 } 
	 catch(Exception e){
		 e.printStackTrace();
	 }
 }
 
 public List<RequestBlood> search(RequestBlood rb){
	 List<RequestBlood> rbs = new ArrayList<RequestBlood>();
	 User user=new User();
	 try {
		   System.out.print("hgkjjkhk");
		   PreparedStatement preparedStatement = connection.prepareStatement("select u.username,r.bg_needed,r.no_of_units,r.location from user u,request r where r.bg_needed=? AND u.userid=r.userid ");
		   preparedStatement.setString(1,rb.getbloodgrp());
		   System.out.print("query");
		   
		   ResultSet rs = preparedStatement.executeQuery();
		   
		    while(rs.next()){
			   RequestBlood rb1=new RequestBlood();
			   user.setUsername("username");
			   System.out.print("hey");
			   rb1.setbloodgrp(rs.getString("bg_needed"));
			    rb1.setamount(rs.getInt("no_of_units"));
			    rb1.setlocation(rs.getString("location"));
			    rbs.add(rb1);
			    System.out.print(user.getUsername());
			    
		       }
	 } catch (SQLException e) {
		   e.printStackTrace();
		  }
		  return rbs;
 }


 public List<RequestBlood> getAllRequests(){
  List<RequestBlood> rbs = new ArrayList<RequestBlood>();
  try {
   Statement statement = connection.createStatement();
   ResultSet rs = statement.executeQuery("select * from request ");
 
   while (rs.next()) {
    RequestBlood rb = new RequestBlood();
    System.out.print("hello");
    rb.setUserid(rs.getInt("userid"));
    rb.setbloodgrp(rs.getString("bg_needed"));
    rb.setamount(rs.getInt("no_of_units"));    
    rb.setlocation(rs.getString("location"));
    rb.setstatus(rs.getString("rstatus"));
    rb.setcordinatorid(rs.getInt("co_ordinator_id"));
    rbs.add(rb);
   }
  } catch (SQLException e) {
   e.printStackTrace();
  }

  return rbs;  
 }
 
 
}

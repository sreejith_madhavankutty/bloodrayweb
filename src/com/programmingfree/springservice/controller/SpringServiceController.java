package com.programmingfree.springservice.controller;

import java.util.Arrays;
import java.util.List;
import java.text.ParseException;

import org.apache.http.protocol.RequestConnControl;
import org.json.JSONException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.programmingfree.springservice.dao.EventService;
import com.programmingfree.springservice.dao.RequestBloodService;
import com.programmingfree.springservice.dao.UserService;
import com.programmingfree.springservice.domain.Check;
import com.programmingfree.springservice.domain.Event;
import com.programmingfree.springservice.domain.RequestBlood;
import com.programmingfree.springservice.domain.User;


@RestController
@RequestMapping("/service/user")
public class SpringServiceController {
 
 UserService userService=new UserService();
 RequestBloodService requestService=new RequestBloodService();
 EventService eventService=new EventService();
 
  @RequestMapping(value = "/{id}", method = RequestMethod.GET)
 public User getUser(@PathVariable int id) throws ParseException {
  User user=userService.getUserById(id);
  return user;
 }
 
 @RequestMapping(method = RequestMethod.GET)
 public List<User> getAllUsers() {
  List<User> users=userService.getAllUsers();
  System.out.println("size  : "+users.size());
  System.out.println(""+users);
  return users;
 }
 
 @RequestMapping(method = RequestMethod.GET)
 public List<Event> getAllEvents() {
  List<Event> events=eventService.getAllEvents();
  System.out.println("size  : "+events.size());
  System.out.println(""+events);
  return events;
 }
 
 @RequestMapping(method=RequestMethod.POST)
 public ResponseEntity<User> insertUsers(@RequestBody User user)throws ParseException{
		System.out.println("hiii");
	
		this.userService.insertUsers(user);
	 
	return new ResponseEntity<User>(user,HttpStatus.OK);
}
 
 @RequestMapping(method=RequestMethod.POST)
 public ResponseEntity<Event> insertEvents(@RequestBody Event event)throws ParseException{
		System.out.println("hiii");
	
		this.eventService.insertEvents(event);
	 
	return new ResponseEntity<Event>(event,HttpStatus.OK);
}
 /*@RequestMapping(value="/check",method=RequestMethod.POST)
 public ResponseEntity<User> check(@RequestBody User user)throws ParseException, JSONException{
		System.out.println("check");
	
		this.userService.check(user);
	 
	return new ResponseEntity<User>(user,HttpStatus.OK);
}*/

/* @RequestMapping(value="/search",method=RequestMethod.POST)
 public ResponseEntity<User> search(@RequestBody User user)throws ParseException{
	 System.out.println("search");
	 User user1 =  userService.search(user);
	 return new ResponseEntity<User>(user1,HttpStatus.OK);
 }*/
 
 @RequestMapping(value="/request",method=RequestMethod.POST)
 public ResponseEntity<RequestBlood> insertRequests(@RequestBody RequestBlood request)throws ParseException{
 	 System.out.println("request");
 	 this.requestService.insertRequests(request);
 	return new ResponseEntity<RequestBlood>(request,HttpStatus.OK);
   }
 
 @RequestMapping(value="/search1",method=RequestMethod.POST)
 public ResponseEntity<List<RequestBlood>> search(@RequestBody RequestBlood request)throws ParseException{
 	 System.out.println("search");
 	// this.requestService.search(request);
 	 List<RequestBlood> req=requestService.search(request);
 	// List<RequestBlood> request = Arrays.asList(new RequestBlood(),new RequestBlood());
 	return new ResponseEntity<List<RequestBlood>>(req,HttpStatus.OK);
 }
  
 @RequestMapping(value="/request1",method=RequestMethod.GET)
 public List<RequestBlood> getAllRequests()throws ParseException{
 	 System.out.println(" search");
 	 List<RequestBlood> request=requestService.getAllRequests();
 	//List<RequestBlood> request = Arrays.asList(new RequestBlood(),new RequestBlood());
 	 //return new ResponseEntity<List<RequestBlood>>(request,HttpStatus.OK);
 	 return request;
 }
 

}

 /*@RequestMapping(value="/{id}/{fName}/{lName}/{Email}",method=RequestMethod.GET)
 public List<User> insertUsers(@PathVariable int id,@PathVariable String fName,@PathVariable String lName,@PathVariable String Email)throws ParseException{
		System.out.println("hiii");
	 User user=new User(); 
	user.setUserid(id);
	user.setFirstName(fName);
	user.setLastName(lName);
	user.setEmail(Email);
	userService.insertUsers(user);
	return userService.getAllUsers();
}
*/
 
 
 

